﻿using Belgrade.SqlClient;
using Microsoft.AspNetCore.Mvc;
using System.Data.SqlClient;
using System.IO;
using System.Threading.Tasks;

namespace FoodieGOAPI.Controllers
{
    [Route("api/[controller]")]
    public class TablesController : Controller
    {
        private readonly IQueryPipe SqlPipe;
        private readonly ICommand SqlCommand;

        public TablesController(ICommand sqlCommand, IQueryPipe sqlPipe)
        {
            this.SqlCommand = sqlCommand;
            this.SqlPipe = sqlPipe;
        }

        public async Task INS_Intruders(string tr_LogsJson)
        {
            string Tr_LogsJson = new StreamReader(Request.Body).ReadToEnd();
            var cmd = new SqlCommand("exec SP_INS_LOGS @Tr_LogsJson");
            cmd.Parameters.AddWithValue("@Tr_LogsJson", tr_LogsJson);
            await SqlPipe.Stream(cmd, Response.Body, "{}");
        }

        [HttpGet("{Username}/{Password}/{dt}/{keys}/{id}/{Merchant_ID}/{Table_ID}")]
        public async Task Get(string Username, string Password, string dt, string keys, int id, double Merchant_ID, int Table_ID)
        {

            if (keys == FoodieGOAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
                var cmd = new SqlCommand("EXEC SP_Get_Tables_ID @Merchant_ID,@Table_ID");
                cmd.Parameters.AddWithValue("Merchant_ID", Merchant_ID);
                cmd.Parameters.AddWithValue("Table_ID", Table_ID);
                await SqlPipe.Stream(cmd, Response.Body, "[]");
            }
            else
            {
                await INS_Intruders(keys);
            }

        }



        [HttpGet("{Username}/{Password}/{dt}/{keys}/{Merchant_ID}")]
        public async Task Get(string Username, string Password, string dt, string keys, double Merchant_ID)
        {

            if (keys == FoodieGOAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
                var cmd = new SqlCommand("EXEC SP_Get_Tables_Search @Merchant_ID");
                cmd.Parameters.AddWithValue("Merchant_ID", Merchant_ID);
                await SqlPipe.Stream(cmd, Response.Body, "[]");
            }
            else
            {
                await INS_Intruders(keys);
            }

        }



        // POST api/Todo
        [HttpPost("{Username}/{Password}/{dt}/{keys}")]
        public async Task Post(string Username, string Password, string dt, string keys)
        {

            if (keys == FoodieGOAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
                string Tr_TablesJson = new StreamReader(Request.Body).ReadToEnd();
                var cmd = new SqlCommand("Exec INS_Table_Orders_Json @Tr_TablesJson");
                cmd.Parameters.AddWithValue("Tr_TablesJson", Tr_TablesJson);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }
            else
            {
                await INS_Intruders(keys);

            }
                                   
        }

        // DELETE api/Todo/5
        [HttpDelete("{Username}/{Password}/{dt}/{keys}")]
        public async Task Delete(string Username,string Password, string dt, string keys)
        {

            if (keys == FoodieGOAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
                string Tr_TablesJson = new StreamReader(Request.Body).ReadToEnd();
                var cmd = new SqlCommand(@"exec Del_Tr_Table_Orders_id @Tr_TablesJson");
                cmd.Parameters.AddWithValue("Tr_TablesJson", Tr_TablesJson);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }
            else
            {
                await INS_Intruders(keys);

            }


        }
    }
}
