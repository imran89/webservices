﻿using Belgrade.SqlClient;
using Microsoft.AspNetCore.Mvc;
using System.Data.SqlClient;
using System.IO;
using System.Threading.Tasks;

namespace FoodieGOAPI.Controllers
{
    [Route("api/[controller]")]
    public class SalesTransController : Controller
    {
        private readonly IQueryPipe SqlPipe;
        private readonly ICommand SqlCommand;

        public SalesTransController(ICommand sqlCommand, IQueryPipe sqlPipe)
        {
            this.SqlCommand = sqlCommand;
            this.SqlPipe = sqlPipe;
        }

        public async Task INS_Intruders(string tr_LogsJson)
        {
            string Tr_LogsJson = new StreamReader(Request.Body).ReadToEnd();
            var cmd = new SqlCommand("exec SP_INS_LOGS @Tr_LogsJson");
            cmd.Parameters.AddWithValue("@Tr_LogsJson", tr_LogsJson);
            await SqlPipe.Stream(cmd, Response.Body, "{}");
        }


       
        // GET api/Todo
        [HttpGet("{Username}/{Password}/{dt}/{keys}/{MerchantID}/{search}")]
        public async Task Get(string Username, string Password, string dt, string keys, int MerchantID, string search)
        {
            if (keys == FoodieGOAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
                var cmd = new SqlCommand("exec SP_Sales_Trans @MerchantID, @search");
                cmd.Parameters.AddWithValue("MerchantID", MerchantID);
                cmd.Parameters.AddWithValue("search", search);
                await SqlPipe.Stream(cmd, Response.Body, "[]");
            }
            else
            {
                await INS_Intruders(keys);

            }
        }
        

        //// DELETE api/Todo/5
        //[HttpDelete("{Username}/{Password}/{dt}/{keys}")]
        //public async Task Delete(string Username, string Password, string dt, string keys)
        //{

        //    if (keys == FoodieGOAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
        //    {
        //        string Tr_SalesJson = new StreamReader(Request.Body).ReadToEnd();
        //        var cmd = new SqlCommand(@"exec Del_Tr_Sales_id @Tr_SalesJson");
        //        cmd.Parameters.AddWithValue("Tr_SalesJson", Tr_SalesJson);
        //        await SqlPipe.Stream(cmd, Response.Body, "{}");
        //    }
        //    else
        //    {
        //        await INS_Intruders(keys);

        //    }


        //}
    }
}
