﻿using Belgrade.SqlClient;
using Microsoft.AspNetCore.Mvc;
using System.Data.SqlClient;
using System.IO;
using System.Threading.Tasks;

namespace FoodieGOAPI.Controllers
{
    [Route("api/[controller]")]
    public class PurchaseController : Controller
    {
        private readonly IQueryPipe SqlPipe;
        private readonly ICommand SqlCommand;

        public PurchaseController(ICommand sqlCommand, IQueryPipe sqlPipe)
        {
            this.SqlCommand = sqlCommand;
            this.SqlPipe = sqlPipe;
        }

        public async Task INS_Intruders(string tr_LogsJson)
        {
            string Tr_LogsJson = new StreamReader(Request.Body).ReadToEnd();
            var cmd = new SqlCommand("exec SP_INS_LOGS @Tr_LogsJson");
            cmd.Parameters.AddWithValue("@Tr_LogsJson", tr_LogsJson);
            await SqlPipe.Stream(cmd, Response.Body, "{}");
        }

        // GET api/Todo/5
        [HttpGet("{Username}/{dt}/{keys}/{Merchant_ID}/{Search}")]
        public async Task Get(string Username, string dt, string keys, double Merchant_ID,string Search)
        {
            if (keys == FoodieGOAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
                var cmd = new SqlCommand("EXEC Sp_getPurchase_Search @merchant_ID,@Search");
                cmd.Parameters.AddWithValue("Merchant_id", Merchant_ID);
                cmd.Parameters.AddWithValue("Search", Search);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }
            else
            {
                await INS_Intruders(keys);

            }

        }

        
        // GET api/Todo/5
        [HttpGet("{Username}/{dt}/{keys}/{id}")]
        public async Task Get(string Username, string dt, string keys,int id)
        {

            if (keys == FoodieGOAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
                var cmd = new SqlCommand("EXEC Sp_getPurchase_id @id");
                cmd.Parameters.AddWithValue("id", id);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }
            else
            {
                await INS_Intruders(keys);

            }
           
        }
    
        // POST api/Todo
        [HttpPost("{Username}/{dt}/{keys}")]
        public async Task Post(string Username, string dt, string keys)
        {

            if (keys == FoodieGOAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
                string Tr_PurchaseJson = new StreamReader(Request.Body).ReadToEnd();
                var cmd = new SqlCommand("Exec Ins_Tr_PurchaseFromJson @Tr_PurchaseJson");
                cmd.Parameters.AddWithValue("@Tr_PurchaseJson", Tr_PurchaseJson);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }
            else
            {
                await INS_Intruders(keys);

            }


           
        }

        // DELETE api/Todo/5
        [HttpDelete("{Username}/{dt}/{keys}")]
        public async Task Delete(string Username, string dt, string keys)
        {

            if (keys == FoodieGOAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
                string Tr_PurchaseJson = new StreamReader(Request.Body).ReadToEnd();
                var cmd = new SqlCommand(@"exec Del_Tr_Purchase_id @Tr_PurchaseJson");
                cmd.Parameters.AddWithValue("Tr_PurchaseJson", @Tr_PurchaseJson);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }
            else
            {
                await INS_Intruders(keys);

            }


        }
    }
}
