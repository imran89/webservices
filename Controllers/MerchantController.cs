﻿using Belgrade.SqlClient;
using Microsoft.AspNetCore.Mvc;
using System.Data.SqlClient;
using System.IO;
using System.Threading.Tasks;

namespace FoodieGOAPI.Controllers
{
    [Route("api/[controller]")]
    public class MerchantController : Controller
    {
        private readonly IQueryPipe SqlPipe;
        private readonly ICommand SqlCommand;

        public MerchantController(ICommand sqlCommand, IQueryPipe sqlPipe)
        {
            this.SqlCommand = sqlCommand;
            this.SqlPipe = sqlPipe;
        }

        // GET api/Todo
        [HttpGet]
        public async Task Get()
        {
            await SqlPipe.Stream("select [dbo].[Get_M_Merchant_Json] (0) ", Response.Body, "[]");
        }

        // GET api/Todo/5
        [HttpGet("{id}")]
        public async Task Get(int id)
        {
            var cmd = new SqlCommand("select [dbo].[Get_M_Merchant_Json](@id)");
            cmd.Parameters.AddWithValue("id", id);
            await SqlPipe.Stream(cmd, Response.Body, "{}");
        }

        // POST api/Todo
        [HttpPost("{Username}/{Password}/{dt}/{keys}")]
        public async Task Post(string Username, string Password, string dt, string keys)
        {
            if (keys == FoodieGOAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
                string M_MerchantsJson = new StreamReader(Request.Body).ReadToEnd();
                var cmd = new SqlCommand(@"EXEC Ins_M_Merchant_Json @M_MerchantsJson");
                cmd.Parameters.AddWithValue("M_MerchantsJson", M_MerchantsJson);
                await SqlCommand.ExecuteNonQuery(cmd);
            }
            else
            {
                await INS_Intruders(keys);

            }
        }

        public async Task INS_Intruders(string tr_LogsJson)
        {
            string Tr_LogsJson = new StreamReader(Request.Body).ReadToEnd();
            var cmd = new SqlCommand("exec SP_INS_LOGS @Tr_LogsJson");
            cmd.Parameters.AddWithValue("@Tr_LogsJson", tr_LogsJson);
            await SqlPipe.Stream(cmd, Response.Body, "{}");
        }

        // PATCH api/Todo
        [HttpPatch]
        public async Task Patch(int id)
        {
            string TR_SalesJson = new StreamReader(Request.Body).ReadToEnd();
            var cmd = new SqlCommand(@"EXEC Upd_M_Merchant_Json @M_MerchantJson");
            cmd.Parameters.AddWithValue("M_MerchantJson", TR_SalesJson);
            await SqlCommand.ExecuteNonQuery(cmd);
        }

        // PUT api/Todo/5
        [HttpPut("{id}")]
        public async Task Put(int id)
        {
            string todo = new StreamReader(Request.Body).ReadToEnd();
            var cmd = new SqlCommand(
        @"update Todo
        set title = json.title, description = json.description,
        completed = json.completed, dueDate = json.dueDate
        from OPENJSON( @todo )
        WITH(   title nvarchar(30), description nvarchar(4000),
                completed bit, dueDate datetime2) AS json
        where Id = @id");
            cmd.Parameters.AddWithValue("id", id);
            cmd.Parameters.AddWithValue("todo", todo);
            await SqlCommand.ExecuteNonQuery(cmd);
        }

        // DELETE api/Todo/5
        [HttpDelete("{id}")]
        public async Task Delete(int id)
        {
            var cmd = new SqlCommand(@"EXEC Del_M_Merchant_Json @id");
            cmd.Parameters.AddWithValue("id", id);
            await SqlCommand.ExecuteNonQuery(cmd);
        }
    }
}
