﻿using Belgrade.SqlClient;
using Microsoft.AspNetCore.Mvc;
using System.Data.SqlClient;
using System.IO;
using System.Threading.Tasks;
using System;
using System.Data;

namespace FoodieGOAPI.Controllers
{
    [Route("api/[controller]")]
    public class ReportController : Controller
    {
        private readonly IQueryPipe SqlPipe;
        private readonly ICommand SqlCommand;

        public ReportController(ICommand sqlCommand, IQueryPipe sqlPipe)
        {
            this.SqlCommand = sqlCommand;
            this.SqlPipe = sqlPipe;
        }

        public async Task INS_Intruders(string tr_LogsJson)
        {
            string Tr_LogsJson = new StreamReader(Request.Body).ReadToEnd();
            var cmd = new SqlCommand("exec SP_INS_LOGS @Tr_LogsJson");
            cmd.Parameters.AddWithValue("@Tr_LogsJson", tr_LogsJson);
            await SqlPipe.Stream(cmd, Response.Body, "{}");
        }

        //// GET api/Todo/5
        //[HttpGet("{Username}/{Password}/{dt}/{keys}/{Module}/{merchant_ID}/{StartPeriode}/{EndPeriode}/{Current}/{Type}/{Consoldation}")]
        //public async Task Get(string Username,string Password, string dt, string keys,string Module, double merchant_ID,DateTime StartPeriode, DateTime EndPeriode, bool Current, string Type, bool Consolidation )
        //{
        //    if (keys == FoodieGOAPI.CLS_FUNCT.Base64Encode(Username + CLS_FUNCT.getpasskey("RestoGOBackend") + dt))
        //    {
        //        if (Module.ToUpper() == "ADJ")
        //        {
        //            var cmd = new SqlCommand("exec SP_RPT_Adjustment @Username, @Merchant_ID, @StartPeriode, @EndPeriode, @Current, @Type, @Consolidation ");
        //            cmd.Parameters.AddWithValue("Username", Username);
        //            cmd.Parameters.AddWithValue("merchant_ID", merchant_ID);
        //            cmd.Parameters.AddWithValue("StartPeriode", StartPeriode);
        //            cmd.Parameters.AddWithValue("EndPeriode", EndPeriode);
        //            cmd.Parameters.AddWithValue("Current", Current);
        //            cmd.Parameters.AddWithValue("Type", Type);
        //            cmd.Parameters.AddWithValue("Consolidation", Consolidation);
        //            await SqlPipe.Stream(cmd, Response.Body, "[]");
        //        }
        //        else if (Module.ToUpper() == "CSH")
        //        {
        //            var cmd = new SqlCommand("exec SP_RPT_Cashflow @Username, @Merchant_ID, @StartPeriode, @EndPeriode, @Current, @Type, @Consolidation ");
        //            cmd.Parameters.AddWithValue("Username", Username);
        //            cmd.Parameters.AddWithValue("merchant_ID", merchant_ID);
        //            cmd.Parameters.AddWithValue("StartPeriode", StartPeriode);
        //            cmd.Parameters.AddWithValue("EndPeriode", EndPeriode);
        //            cmd.Parameters.AddWithValue("Current", Current);
        //            cmd.Parameters.AddWithValue("Type", Type);
        //            cmd.Parameters.AddWithValue("Consolidation", Consolidation);
        //            await SqlPipe.Stream(cmd, Response.Body, "[]");
        //        }
        //        else if (Module.ToUpper() == "OPN")
        //        {
        //            var cmd = new SqlCommand("exec SP_RPT_Opname @Username, @Merchant_ID, @StartPeriode, @EndPeriode, @Current, @Type, @Consolidation ");
        //            cmd.Parameters.AddWithValue("Username", Username);
        //            cmd.Parameters.AddWithValue("merchant_ID", merchant_ID);
        //            cmd.Parameters.AddWithValue("StartPeriode", StartPeriode);
        //            cmd.Parameters.AddWithValue("EndPeriode", EndPeriode);
        //            cmd.Parameters.AddWithValue("Current", Current);
        //            cmd.Parameters.AddWithValue("Type", Type);
        //            cmd.Parameters.AddWithValue("Consolidation", Consolidation);
        //            await SqlPipe.Stream(cmd, Response.Body, "[]");
        //        }
        //        else if (Module.ToUpper() == "PCH")
        //        {
        //            var cmd = new SqlCommand("exec SP_RPT_Purchase @Username, @Merchant_ID, @StartPeriode, @EndPeriode, @Current, @Type, @Consolidation ");
        //            cmd.Parameters.AddWithValue("Username", Username);
        //            cmd.Parameters.AddWithValue("merchant_ID", merchant_ID);
        //            cmd.Parameters.AddWithValue("StartPeriode", StartPeriode);
        //            cmd.Parameters.AddWithValue("EndPeriode", EndPeriode);
        //            cmd.Parameters.AddWithValue("Current", Current);
        //            cmd.Parameters.AddWithValue("Type", Type);
        //            cmd.Parameters.AddWithValue("Consolidation", Consolidation);
        //            await SqlPipe.Stream(cmd, Response.Body, "[]");
        //        }
        //        else if (Module.ToUpper() == "SLS")
        //        {
        //            var cmd = new SqlCommand("exec SP_RPT_Sales @Username, @Merchant_ID, @StartPeriode, @EndPeriode, @Current, @Type, @Consolidation ");
        //            cmd.Parameters.AddWithValue("Username", Username);
        //            cmd.Parameters.AddWithValue("merchant_ID", merchant_ID);
        //            cmd.Parameters.AddWithValue("StartPeriode", StartPeriode);
        //            cmd.Parameters.AddWithValue("EndPeriode", EndPeriode);
        //            cmd.Parameters.AddWithValue("Current", Current);
        //            cmd.Parameters.AddWithValue("Type", Type);
        //            cmd.Parameters.AddWithValue("Consolidation", Consolidation);
        //            await SqlPipe.Stream(cmd, Response.Body, "[]");
        //        }
        //    }
        //    else
        //    {
        //        await INS_Intruders(keys);

        //    }

        //}
        // GET api/Todo/5
        [HttpGet("{Username}/{Password}/{dt}/{keys}/{App}/{module}/{merchant_ID}/{StartPeriode}/{EndPeriode}/{Current}/{Type}/{Consolidation}")]
        public async Task Get(string Username, string Password, string dt, string keys, string App, string Module, double Merchant_ID, DateTime StartPeriode, DateTime EndPeriode, bool Current, string Type, bool Consolidation)
        {
            //if (App == "ServGO")
            //{
            //    s = s.Replace("Database=foodieGO", "Database=ServGO");
            //}
            //else if (App == "StoreGO")
            //{
            //    s = s.Replace("Database=foodieGO", "Database=StoreGO");
            //}
            string query;

            if (Module.ToUpper() == "SLS")
            {
                query = "SP_RPT_Sales";
            }
            else if (Module.ToUpper() == "PCS")
            {
                query = "SP_RPT_Purchase";
            }
            else if (Module.ToUpper() == "OPN")
            {
                query = "SP_RPT_Opname";
            }
            else if (Module.ToUpper() == "INV")
            {
                query = "SP_RPT_Inventory";
            }
            else if (Module.ToUpper() == "CSH")
            {
                query = "SP_RPT_Cashflow";
            }
            else
            {
                query = "SP_RPT_Adjustment";
            }

            if (keys == FoodieGOAPI.CLS_FUNCT.Base64Encode(Username + CLS_FUNCT.getpasskey("RestoGOBackend") + dt))
            {

                var cmd = new SqlCommand(query);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("username", Username);
                cmd.Parameters.AddWithValue("Merchant_ID", Merchant_ID);
                cmd.Parameters.AddWithValue("StartPeriode", StartPeriode);
                cmd.Parameters.AddWithValue("EndPeriode", EndPeriode);
                cmd.Parameters.AddWithValue("Current", Current);
                cmd.Parameters.AddWithValue("Type", Type);
                cmd.Parameters.AddWithValue("Consolidation", Consolidation);
                await SqlPipe.Stream(cmd, Response.Body, "[]");
            }
            else
            {
                await INS_Intruders(keys);

            }

        }


    }
}