﻿using Belgrade.SqlClient;
using Microsoft.AspNetCore.Mvc;
using System.Data.SqlClient;
using System.IO;
using System.Threading.Tasks;

namespace FoodieGOAPI.Controllers
{
    [Route("api/[controller]")]
    public class CashflowController : Controller
    {
        private readonly IQueryPipe SqlPipe;
        private readonly ICommand SqlCommand;

        public CashflowController(ICommand sqlCommand, IQueryPipe sqlPipe)
        {
            this.SqlCommand = sqlCommand;
            this.SqlPipe = sqlPipe;
        }

        public async Task INS_Intruders(string tr_LogsJson)
        {
            string Tr_LogsJson = new StreamReader(Request.Body).ReadToEnd();
            var cmd = new SqlCommand("exec SP_INS_LOGS @Tr_LogsJson");
            cmd.Parameters.AddWithValue("@Tr_LogsJson", tr_LogsJson);
            await SqlPipe.Stream(cmd, Response.Body, "{}");
        }

        // GET api/Todo/5
        [HttpGet("{Username}/{Password}/{dt}/{keys}/{id}")]
        public async Task Get(string Username,string Password, string dt, string keys,double id)
        {
            if (keys == FoodieGOAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
                var cmd = new SqlCommand("exec SP_Get_Cashflow_ID @id");
                cmd.Parameters.AddWithValue("id", id);
                await SqlPipe.Stream(cmd, Response.Body, "[]");
            }
            else
            {
                await INS_Intruders(keys);

            }

        }

        
        // GET api/Todo/5
        [HttpGet("{Username}/{Password}/{dt}/{keys}/{Merchant_id}/{search}")]
        public async Task Get(string Username, string Password, string dt, string keys,double Merchant_id,string Search)
        {

            if (keys == FoodieGOAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
                var cmd = new SqlCommand("EXEC SP_Get_Cashflow_Search @Merchant_ID, @search");
                cmd.Parameters.AddWithValue("Merchant_id", Merchant_id);
                cmd.Parameters.AddWithValue("search", Search);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }
            else
            {
                await INS_Intruders(keys);
            }
           
        }
    
        // POST api/Todo
        [HttpPost("{Username}/{Password}/{dt}/{keys}")]
        public async Task Post(string Username, string Password, string dt, string keys)
        {

            if (keys == FoodieGOAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
                string Tr_CashflowJson = new StreamReader(Request.Body).ReadToEnd();
                var cmd = new SqlCommand("Exec Ins_Tr_Cashflow_Json @Tr_CashflowJson");
                cmd.Parameters.AddWithValue("Tr_CashflowJson", Tr_CashflowJson);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }
            else
            {
                await INS_Intruders(keys);

            }


           
        }

        // DELETE api/Todo/5
        [HttpDelete("{Username}/{Password}/{dt}/{keys}")]
        public async Task Delete(string Username,string Password, string dt, string keys)
        {

            if (keys == FoodieGOAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
                string Tr_CashflowJson = new StreamReader(Request.Body).ReadToEnd();
                var cmd = new SqlCommand(@"exec Del_Tr_Cashflow_id @Tr_CashflowJson");
                cmd.Parameters.AddWithValue("Tr_CashflowJson", Tr_CashflowJson);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }
            else
            {
                await INS_Intruders(keys);

            }


        }
    }
}
