﻿using Belgrade.SqlClient;
using Microsoft.AspNetCore.Mvc;
using System.Data.SqlClient;
using System.IO;
using System.Threading.Tasks;
using FoodieGOAPI;


namespace FoodieGOAPI.Controllers
{
    [Route("api/[controller]")]
    public class CheckController : Controller
    {
        private readonly IQueryPipe SqlPipe;
        private readonly ICommand SqlCommand;

        public CheckController(ICommand sqlCommand, IQueryPipe sqlPipe)
        {
            this.SqlCommand = sqlCommand;
            this.SqlPipe = sqlPipe;
        }
        

        [HttpGet("{Username}/{Password}/{dt}/{keys}/{id}/{Merchant_ID}")]
        public async Task Get(string Username, string Password, string dt, string keys, double id, double Merchant_ID)
        {
            if (keys == FoodieGOAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
                var cmd = new SqlCommand("Exec SP_get_CheckPoints @id, @Merchant_ID ");
                cmd.Parameters.AddWithValue("id", id);
                cmd.Parameters.AddWithValue("Merchant_ID", Merchant_ID);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }
            else
            {
                await INS_Intruders(Username);

            }
        }


        public async Task INS_Intruders(string tr_LogsJson)
        {
            string Tr_LogsJson = new StreamReader(Request.Body).ReadToEnd();
            var cmd = new SqlCommand("exec SP_INS_LOGS @Tr_LogsJson");
            cmd.Parameters.AddWithValue("@Tr_LogsJson", tr_LogsJson);
            await SqlPipe.Stream(cmd, Response.Body, "{}");
        }


        // POST api/Todo
        [HttpPost("{Username}/{Password}/{dt}/{keys}")]
        public async Task Post(string Username, string Password, string dt, string keys)
        {

            if (keys == FoodieGOAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {

                string Tr_checkPointsJson = new StreamReader(Request.Body).ReadToEnd();
            // INS_Intruders(LoginJson);
            var cmd = new SqlCommand("Exec Ins_Tr_checkPoints_Json @Tr_checkPointsJson");
            cmd.Parameters.AddWithValue("@Tr_checkPointsJson", Tr_checkPointsJson);
            await SqlPipe.Stream(cmd, Response.Body, "{}");

            }
            else
            {
                INS_Intruders(keys);

            }


        }


    }
}
