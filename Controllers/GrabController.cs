﻿using Belgrade.SqlClient;
using Microsoft.AspNetCore.Mvc;
using System.Data.SqlClient;
using System.IO;
using System.Threading.Tasks;

namespace FoodieGOAPI.Controllers
{
    [Route("api/[controller]")]
    public class GrabController : Controller
    {
        private readonly IQueryPipe SqlPipe;
        private readonly ICommand SqlCommand;

        public GrabController(ICommand sqlCommand, IQueryPipe sqlPipe)
        {
            this.SqlCommand = sqlCommand;
            this.SqlPipe = sqlPipe;
        }

        public async Task INS_Intruders(string tr_LogsJson)
        {
            string Tr_LogsJson = new StreamReader(Request.Body).ReadToEnd();
            var cmd = new SqlCommand("exec SP_INS_LOGS @Tr_LogsJson");
            cmd.Parameters.AddWithValue("@Tr_LogsJson", tr_LogsJson);
            await SqlPipe.Stream(cmd, Response.Body, "{}");
        }

        // GET api/Todo/5
        [HttpGet("{Username}/{Password}/{dt}/{keys}/{id}/{merchant_ID}")]
        public async Task Get(string Username,string Password, string dt, string keys,double id,double merchant_ID)
        {
            if (keys == FoodieGOAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
              

                var cmd = new SqlCommand("exec SP_Get_Location_ID @id,@merchant_ID");
                cmd.Parameters.AddWithValue("id", id);
                cmd.Parameters.AddWithValue("merchant_ID", merchant_ID);
                await SqlPipe.Stream(cmd, Response.Body, "[]");
            }
            else
            {
                await INS_Intruders(keys);

            }

        }

        
        // GET api/Todo/5
        [HttpGet("{Username}/{Password}/{dt}/{keys}/{id}/{merchant_ID}/{search}")]
        public async Task Get(string Username, string Password, string dt, string keys,int id,double merchant_ID,string Search)
        {

            if (keys == FoodieGOAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
                var cmd = new SqlCommand("EXEC SP_Get_Location_Search @search, @Merchant_ID");
                cmd.Parameters.AddWithValue("search", Search);
                cmd.Parameters.AddWithValue("Merchant_ID", merchant_ID);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }
            else
            {
                await INS_Intruders(keys);

            }
           
        }
    
        // POST api/Todo
        [HttpPost("{Username}/{Password}/{dt}/{keys}")]
        public string Post(string Username, string Password, string dt, string keys)
        {

            if (keys == FoodieGOAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
                try
                {
                    string SecretKey = "iQtAg85rR9Z0Q9Q6";
                    string base64_encoded_hmac_signature;
                    string stringToSign;
                    string hmac_signature;

                    string M_GrabDeliveryJson = new StreamReader(Request.Body).ReadToEnd();

                    string stringTemp = CLS_FUNCT.Base64Encode(CLS_FUNCT.sha256(M_GrabDeliveryJson));

                    string Tanggal = System.DateTime.UtcNow.AddMinutes(55).ToString("dd MM yyyy HH:mm:ss");

                    stringToSign = "HTTPStatusCode" + "\n" + Tanggal + "\n" + stringTemp + "\n";

                    //test
                    hmac_signature = CLS_FUNCT.CreateToken(SecretKey, stringToSign);

                    //encoded last
                    base64_encoded_hmac_signature = CLS_FUNCT.Base64Encode(hmac_signature);

                    return @"{""Status"":""Success"",""base64_encoded_hmac_signature"":""" + base64_encoded_hmac_signature + "}";


                }
                catch (System.Exception ex)
                {
                    return @"{""Status"":""failed"",""Desc:""" + ex.Message.ToString() + "}";

                }
                
                }
            else
            {
                INS_Intruders(keys);
                return @"{""Status"" : ""Failed""}";



            }


           
        }

        // DELETE api/Todo/5
        [HttpDelete("{Username}/{Password}/{dt}/{keys}")]
        public async Task Delete(string Username,string Password, string dt, string keys)
        {

            if (keys == FoodieGOAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
                string M_LocationsJson = new StreamReader(Request.Body).ReadToEnd();
                var cmd = new SqlCommand(@"exec Del_M_Locations @M_LocationsJson");
                cmd.Parameters.AddWithValue("M_LocationsJson", M_LocationsJson);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }
            else
            {
                await INS_Intruders(keys);

            }


        }
    }
}
