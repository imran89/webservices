﻿using Belgrade.SqlClient;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Threading.Tasks;

namespace FoodieGOAPI.Controllers
{
    [Route("api/[controller]")]
    public class LoginController : Controller
    {
        private readonly IQueryPipe SqlPipe;
        private readonly ICommand SqlCommand;

        public LoginController(ICommand sqlCommand, IQueryPipe sqlPipe)
        {
            this.SqlCommand = sqlCommand;
            this.SqlPipe = sqlPipe;
        }

        public async Task INS_Intruders(string tr_LogsJson)
        {
            string Tr_LogsJson = new StreamReader(Request.Body).ReadToEnd();
            var cmd = new SqlCommand("exec SP_INS_LOGS @Tr_LogsJson");
            cmd.Parameters.AddWithValue("@Tr_LogsJson", tr_LogsJson);
            await SqlPipe.Stream(cmd, Response.Body, "{}");
        }


        // GET api/Todo/5
        [HttpGet("{Username}/{Password}/{dt}/{keys}/{Module}/{App}")]
        public string Get(string Username, string Password, string dt, string keys, string Module, string App)
        {
            if (keys == FoodieGOAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
                SqlConnection conn = null;
                SqlDataReader rdr = null;
                try
                {
                    conn = new SqlConnection("Server=foodieGO.database.windows.net;Database=FoodieGO;User Id=thomas.benny;Password=Mcfurry.2011");
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("dbo.SP_GET_EMAIL", conn);
                    cmd.Parameters.AddWithValue("@Module", Module);
                    cmd.Parameters.AddWithValue("@App", App);
                    cmd.Parameters.AddWithValue("@Username", Username);
                    cmd.Parameters.AddWithValue("@dt", dt);
                    cmd.Parameters.AddWithValue("@keys", keys);
                    cmd.CommandType = CommandType.StoredProcedure;
                    rdr = cmd.ExecuteReader();
                    while (rdr.Read())
                    {
                        FoodieGOAPI.MessageServices.SendEmailAsync(Username, rdr["title"].ToString(), rdr["bodyHTML"].ToString(), rdr["bodyText"].ToString());

                    }
                    return @"{""status"":""email has been sent.""}";
                }
                catch (Exception ex)
                {

                    return @"{""status"":""email not found." +ex.Message.ToString()+" }";
                    //return ex.Message.ToString();
                }
                finally
                {

                    if (conn != null)
                    {
                        conn.Close();
                    }
                    if (rdr != null)
                    {
                        rdr.Dispose();
                    }
                }
            }
            else
            {
                INS_Intruders(Username);
                return @"{""status"":""Intruders.""}";

            }
        }

        // GET api/Todo/5
        [HttpGet("{Username}/{Password}/{dt}/{keys}/{Merchant_ID}/{PeriodeStr}/{Type}")]
        public async Task Get(string Username, string Password, string dt, string keys,double Merchant_ID,string PeriodeStr, string Type)
        {
            if (keys == FoodieGOAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
                var cmd = new SqlCommand("exec SP_Operator_Profile @Merchant_ID,@Username,@PeriodeStr,@Type");
                cmd.Parameters.AddWithValue("Merchant_ID", Merchant_ID);
                cmd.Parameters.AddWithValue("Username", Username);
                cmd.Parameters.AddWithValue("PeriodeStr", PeriodeStr);
                cmd.Parameters.AddWithValue("Type", Type);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }
            else
            {
                await INS_Intruders(keys);

            }

        }


        // GET api/Todo/5
        [HttpGet("{Username}/{Password}/{dt}/{keys}")]
        public async Task Get(string Username,string Password, string dt, string keys)
        {
            if (keys == FoodieGOAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
                var cmd = new SqlCommand("exec SP_Get_Logins_ID @username,@password");
                cmd.Parameters.AddWithValue("username", Username);
                cmd.Parameters.AddWithValue("password", Password);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }
            else
            {
                await INS_Intruders(keys);

            }

        }

        // GET api/Todo/5
        [HttpGet("{Username}/{Password}/{dt}/{keys}/{Parameter}")]
        public async Task Get(string Username, string Password, string dt, string keys, string Parameter)
        {
            if (keys == FoodieGOAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
                if (Parameter == "Checkusername")
                {
                    var cmd = new SqlCommand("exec SP_Get_Check_ID @username");
                    cmd.Parameters.AddWithValue("username", Username);
                    await SqlPipe.Stream(cmd, Response.Body, "{}");
                }
                else
                {
                    
                }
            }
            else
            {
                await INS_Intruders(keys);

            }

        }


    }
}
