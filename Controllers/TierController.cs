﻿using Belgrade.SqlClient;
using Microsoft.AspNetCore.Mvc;
using System.Data.SqlClient;
using System.IO;
using System.Threading.Tasks;

namespace FoodieGOAPI.Controllers
{
    [Route("api/[controller]")]
    public class TierController : Controller
    {
        private readonly IQueryPipe SqlPipe;
        private readonly ICommand SqlCommand;

        public TierController(ICommand sqlCommand, IQueryPipe sqlPipe)
        {
            this.SqlCommand = sqlCommand;
            this.SqlPipe = sqlPipe;
        }

        // GET api/Todo
        [HttpGet]
        public async Task Get()
        {
            await SqlPipe.Stream("SELECT M_get_M_Tiers_Json 0 ", Response.Body, "[]");
        }

        // GET api/Todo/5
        [HttpGet("{id}")]
        public async Task Get(int id)
        {
            var cmd = new SqlCommand("SELECT M_get_M_Tiers_Json @id");
            cmd.Parameters.AddWithValue("id", id);
            await SqlPipe.Stream(cmd, Response.Body, "{}");
        }
    
        // POST INSERT
        [HttpPost]
        public async Task Post()
        {
            string tiers = new StreamReader(Request.Body).ReadToEnd();
            var cmd = new SqlCommand(@"EXEC INS_M_Tiers_Json @tiers");
            cmd.Parameters.AddWithValue("tiers", tiers);
            await SqlCommand.ExecuteNonQuery(cmd);
        }

        // PATCH UPDATE
        [HttpPatch]
        public async Task Patch(int id)
        {
            string tiers = new StreamReader(Request.Body).ReadToEnd();
            var cmd = new SqlCommand(@"EXEC Upd_M_Tiers_Json @tiers");
            cmd.Parameters.AddWithValue("tiers", tiers);
            await SqlCommand.ExecuteNonQuery(cmd);
        }

        // PUT api/Todo/5
        [HttpPut("{id}")]
        public async Task Put(int tiers)
        {
            string todo = new StreamReader(Request.Body).ReadToEnd();
            var cmd = new SqlCommand(@"EXEC Upd_M_Tiers_Json @tiers");
            cmd.Parameters.AddWithValue("tiers", tiers);
            await SqlCommand.ExecuteNonQuery(cmd);
        }

        // DELETE api/Todo/5
        [HttpDelete("{id}")]
        public async Task Delete(int tiers)
        {
            var cmd = new SqlCommand(@"EXEC Del_M_Tiers_Json @tiers");
            cmd.Parameters.AddWithValue("tiers", tiers);
            await SqlCommand.ExecuteNonQuery(cmd);
        }
    }
}
