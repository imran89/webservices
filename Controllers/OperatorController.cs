﻿using Belgrade.SqlClient;
using Microsoft.AspNetCore.Mvc;
using System.Data.SqlClient;
using System.IO;
using System.Threading.Tasks;

namespace FoodieGOAPI.Controllers
{
    [Route("api/[controller]")]
    public class OperatorController : Controller
    {
        private readonly IQueryPipe SqlPipe;
        private readonly ICommand SqlCommand;

        public OperatorController(ICommand sqlCommand, IQueryPipe sqlPipe)
        {
            this.SqlCommand = sqlCommand;
            this.SqlPipe = sqlPipe;
        }

        public async Task INS_Intruders(string tr_LogsJson)
        {
            string Tr_LogsJson = new StreamReader(Request.Body).ReadToEnd();
            var cmd = new SqlCommand("exec SP_INS_LOGS @Tr_LogsJson");
            cmd.Parameters.AddWithValue("@Tr_LogsJson", tr_LogsJson);
            await SqlPipe.Stream(cmd, Response.Body, "{}");
        }

        // GET api/Todo/5
        [HttpGet("{Username}/{Password}/{dt}/{keys}/{id}")]
        public async Task Get(string Username,string Password, string dt, string keys,double id)
        {
            if (keys == FoodieGOAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
                var cmd = new SqlCommand("exec sp_get_operators_ID @id");
                cmd.Parameters.AddWithValue("id", id);
                await SqlPipe.Stream(cmd, Response.Body, "[]");
            }
            else
            {
                await INS_Intruders(keys);

            }

        }

        // GET api/Todo/5
        [HttpGet("{Username}/{Password}/{dt}/{keys}")]
        public async Task Get(string Username, string Password, string dt, string keys)
        {
            if (keys == FoodieGOAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
                var cmd = new SqlCommand("exec SP_Profile @Username");
                cmd.Parameters.AddWithValue("Username", Username);
                await SqlPipe.Stream(cmd, Response.Body, "[]");
            }
            else
            {
                await INS_Intruders(keys);

            }

        }
        
        // GET api/Todo/5
        [HttpGet("{Username}/{Password}/{dt}/{keys}/{Merchant_ID}/{search}")]
        public async Task Get(string Username, string Password, string dt, string keys,double Merchant_ID, string Search)
        {

            if (keys == FoodieGOAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
                var cmd = new SqlCommand("EXEC SP_get_Operators_Search @search,@Merchant_ID");
                cmd.Parameters.AddWithValue("search", Search);
                cmd.Parameters.AddWithValue("Merchant_ID", Merchant_ID);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }
            else
            {
                await INS_Intruders(keys);

            }
           
        }
    
        // POST api/Todo
        [HttpPost("{Username}/{Password}/{dt}/{keys}")]
        public async Task Post(string Username, string Password, string dt, string keys)
        {

            if (keys == FoodieGOAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
                string M_OperatorsJson = new StreamReader(Request.Body).ReadToEnd();
                var cmd = new SqlCommand("Exec Ins_M_OperatorsFromJson @M_OperatorsJson");
                cmd.Parameters.AddWithValue("M_OperatorsJson", M_OperatorsJson);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }
            else
            {
                await INS_Intruders(keys);

            }


           
        }

        // DELETE api/Todo/5
        [HttpDelete("{Username}/{Password}/{dt}/{keys}")]
        public async Task Delete(string Username,string Password, string dt, string keys)
        {

            if (keys == FoodieGOAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
                string M_OperatorsJson = new StreamReader(Request.Body).ReadToEnd();
                var cmd = new SqlCommand(@"exec Del_M_Operators @M_OperatorsJson");
                cmd.Parameters.AddWithValue("M_OperatorsJson", M_OperatorsJson);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }
            else
            {
                await INS_Intruders(keys);

            }


        }
    }
}
