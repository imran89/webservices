﻿using Belgrade.SqlClient;
using Microsoft.AspNetCore.Mvc;
using System.Data.SqlClient;
using System.IO;
using System.Threading.Tasks;

namespace FoodieGOAPI.Controllers
{
    [Route("api/[controller]")]
    public class VersionController : Controller
    {
        private readonly IQueryPipe SqlPipe;
        private readonly ICommand SqlCommand;

        public VersionController(ICommand sqlCommand, IQueryPipe sqlPipe)
        {
            this.SqlCommand = sqlCommand;
            this.SqlPipe = sqlPipe;
        }

        // GET api/Todo
        [HttpGet]
        public string  Get()
        {
            //return "Version 1.6 - Item_Merchants";
            //return "Version 1.7.2 - Login-RestoGO";
            //return "Version 1.8 - Inventory";
            //return "Version 1.9 - Cashflow";
            //return "Version 2.0 - BOM";
            //return "Version 2.1 - PAYMENTS - Rev A, ID & Search Get";
            //return "Version 2.2 - ADJUSTMENT";
            //return "Version 2.3 - SALES ";
            //return "Version 2.4 - BOM ";
            // return "Version 2.5 - Product Parameter";
            //return "Version 2.6 - Opname";
            //return "Version 2.7 - Supplier";
            //return "Version 2.8 - Tables";
            //return "Version 2.9 - ItemGroups";
            //return "Version 2.10 - Locations";
            //return "Version 2.11 - Accounts";
            //return "Version 2.12 - TableOrder";
            //return "Version 2.13 - Grab Express";
            //return "Version 2.14 - Customers";
            //return "Version 2.15 - Username-Check";
            //return "Version 2.16 - Products";
            //return "Version 2.17 - Settings";
            //return "Version 2.19 - SalesTrans";
            //return "Version 2.19 - Upd Product ";
            //return "Version 2.20 - Cashier"; 
            //return "Version 2.21 - Confirm Order";
            //return "Version 2.22 - Tambahan Payment Report (Emoney or Cash)";
            //return "Version 2.23 - Dashboard";
            //return "Version 2.24 - Report and secreetkey";
            //return @"{""Version_Code"":""2.25"",""Version_Name"":""Report""}";
            //return @"{""Version_Code"":""2.26"",""Version_Post"":""Report""}";
            //return @"{""Version_Code"":""2.27"",""Version_Post"":""Test""}"; 
            //return @"{""Version_Code"":""2.28"",""Version_Post"":""HistoryOfSalesController, Using parameter Format Json""}";
            //return @"{""Version_Code"":""2.29"",""Version_Post"":""HistoryOfSalesController, Using parameter Format Json, Revisi SP Name""}";
            //return @"{""Version_Code"":""2.30"",""Version_Get"":""HistoryOfSalesController, Using parameter Format Json (EnCode Base64), Revisi SP Name""}";
            //return @"{""Version_Code"":""2.31"",""Version_Get"":""Update Sales DeliveryID 1""}";
            //return @"{""Version_Code"":""2.31"",""Version_Get"":""Stock Items""}";
            return @"{""Version_Code"":""2.32"",""Version_Get"":""Varian""}";
        }

        //[HttpGet("{Kunci}")]
        //public string Get_Key(string Kunci)
        //{
        //    return CLS_FUNCT.getpasskey(Kunci);
        //}

            
        

    }
}
