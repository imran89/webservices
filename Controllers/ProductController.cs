﻿using Belgrade.SqlClient;
using Microsoft.AspNetCore.Mvc;
using System.Data.SqlClient;
using System.IO;
using System.Threading.Tasks;

namespace FoodieGOAPI.Controllers
{
    [Route("api/[controller]")]
    public class ProductController : Controller
    {
        private readonly IQueryPipe SqlPipe;
        private readonly ICommand SqlCommand;


        public async Task INS_Intruders(string tr_LogsJson)
        {
            string Tr_LogsJson = new StreamReader(Request.Body).ReadToEnd();
            var cmd = new SqlCommand("exec SP_INS_LOGS @Tr_LogsJson");
            cmd.Parameters.AddWithValue("@Tr_LogsJson", tr_LogsJson);
            await SqlPipe.Stream(cmd, Response.Body, "{}");
        }

        public ProductController(ICommand sqlCommand, IQueryPipe sqlPipe)
        {
            this.SqlCommand = sqlCommand;
            this.SqlPipe = sqlPipe;
        }

        // GET api/Todo
        [HttpGet]
        public async Task Get()
        {
            await SqlPipe.Stream("EXEC sp_getProduct ", Response.Body, "[]");
        }

        //// GET api/Todo/5
        //[HttpGet("{id}")]
        //public async Task Get(int id)
        //{
        //    //if (keys == FoodieGOAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
        //    //{
        //        var cmd = new SqlCommand("EXEC sp_getProduct @id");
        //    cmd.Parameters.AddWithValue("id", id);
        //    await SqlPipe.Stream(cmd, Response.Body, "{}");
        //    //}
        //    //else
        //    //{
        //    //    await INS_Intruders(keys);

        //    //}
        //}


        // GET api/Todo/5
        [HttpGet("{Username}/{Password}/{dt}/{keys}/{id}")]
        public async Task Get(string Username, string Password, string dt, string keys, double id)
        {
            if (keys == FoodieGOAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
                var cmd = new SqlCommand("exec SP_get_Products_ID @id");
                cmd.Parameters.AddWithValue("id", id);
                await SqlPipe.Stream(cmd, Response.Body, "[]");
            }
            else
            {
                await INS_Intruders(keys);

            }

        }


        //// GET api/Todo/5
        //[HttpGet("{Username}/{Password}/{dt}/{keys}/{id}/{Merchant_ID}/{Search}")]
        //public async Task Get(string Username, string Password, string dt, string keys, double id, double Merchant_ID, string Search)
        //{
        //    if (keys == FoodieGOAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
        //    {
        //        var cmd = new SqlCommand("exec SP_get_Products_ID_Search @Merchant_ID, @id, @Search");
        //        cmd.Parameters.AddWithValue("Merchant_ID", Merchant_ID);
        //        cmd.Parameters.AddWithValue("id", id);
        //        cmd.Parameters.AddWithValue("Search", Search);
        //        await SqlPipe.Stream(cmd, Response.Body, "[]");
        //    }
        //    else
        //    {
        //        await INS_Intruders(keys);

        //    }

        //}



        //// GET api/Todo/5
        //[HttpGet("{Username}/{Password}/{dt}/{keys}/{Merchant_ID}/{search}")]
        //public async Task Get(string Username, string Password, string dt, string keys, double Merchant_ID, string Search)
        //{

        //    if (keys == FoodieGOAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
        //    {
        //        var cmd = new SqlCommand("EXEC SP_get_Products_Search @Merchant_ID, @search");
        //        cmd.Parameters.AddWithValue("Merchant_ID", Merchant_ID);
        //        cmd.Parameters.AddWithValue("search", Search);
        //        await SqlPipe.Stream(cmd, Response.Body, "{}");
        //    }
        //    else
        //    {
        //        await INS_Intruders(keys);

        //    }

        //}

        // GET api/Todo/5 FOR CINTAID/PRODUCTS/ Item_Merchant_Get_Merchant_ID 
        [HttpGet("{Username}/{Password}/{dt}/{keys}/{MerchantApps}/{Merchant_ID}/{search}")]
        public async Task Get(string Username, string Password, string dt, string keys, string MerchantApps, double Merchant_ID, string search)
        {

            if (keys == FoodieGOAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
                var cmd = new SqlCommand("EXEC SP_get_Products_Search @MerchantApps, @Merchant_ID, @search");
                cmd.Parameters.AddWithValue("MerchantApps", MerchantApps);
                cmd.Parameters.AddWithValue("Merchant_ID", Merchant_ID);
                cmd.Parameters.AddWithValue("search", search);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }
            else
            {
                await INS_Intruders(keys);

            }

        }


        //// POST api/Todo
        //[HttpPost]
        //public async Task Post()
        //{
        //    string MT_Products_MerchantsJson = new StreamReader(Request.Body).ReadToEnd();
        //    var cmd = new SqlCommand(@"EXEC [Ins_MT_Products_MerchantsFromJson] @MT_Products_MerchantsJson");
        //    cmd.Parameters.AddWithValue("@MT_Products_MerchantsJson", MT_Products_MerchantsJson);
        //    await SqlCommand.ExecuteNonQuery(cmd);
        //}


        //// DELETE api/Todo/5
        //[HttpDelete("{id}")]
        //public async Task Delete(int id)
        //{
        //    var cmd = new SqlCommand(@"Del_MT_Products_Merchants @Product_ID");
        //    cmd.Parameters.AddWithValue("@Product_ID", id);
        //    await SqlCommand.ExecuteNonQuery(cmd);
        //}
    }
}
