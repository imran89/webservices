﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace FoodieGOAPI
{
    public class CLS_FUNCT
    {

        public static string CreateToken(string message, string secret)
        {
            secret = secret ?? "";
            string Temp;
            var encoding = new System.Text.ASCIIEncoding();
            byte[] keyByte = encoding.GetBytes(secret);
            byte[] messageBytes = encoding.GetBytes(message);
            using (var hmacsha256 = new System.Security.Cryptography.HMACSHA256(keyByte))
            {
                byte[] hashmessage = hmacsha256.ComputeHash(messageBytes);
                Temp = Convert.ToBase64String(hashmessage);

            }
            return Temp;
        }

        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }


        public static string sha256(string randomString)
        {
            using (var sha256 = SHA256.Create())
            {
                // Send a sample text to hash.  
                var hashedBytes = sha256.ComputeHash(System.Text.Encoding.UTF8.GetBytes(randomString));
                // Get the hashed string.  
                var hash = BitConverter.ToString(hashedBytes).Replace("-", "").ToLower();
                // Print the string.   
                return hash.ToString();
            }


            //var crypt = new System.Security.Cryptography.SHA256Managed();
            //var hash = new System.Text.StringBuilder();
            //byte[] crypto = crypt.ComputeHash(System.Text.Encoding.UTF8.GetBytes(randomString));
            //foreach (byte theByte in crypto)
            //{
            //    hash.Append(theByte.ToString("x2"));
            //}
           // return hash.ToString();
        }

        public static string getpasskey(string kunci)
        {

            try
            {
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
                builder.DataSource = "foodiego.database.windows.net";
                builder.UserID = "thomas.benny";
                builder.Password = "Mcfurry.2011";
                builder.InitialCatalog = "FoodieGO";
                SqlDataReader rdr = null;
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();
                    SqlCommand cmd = new SqlCommand("dbo.SP_Get_Key", connection);
                    cmd.Parameters.AddWithValue("@Kunci", kunci);
                    cmd.CommandType = CommandType.StoredProcedure;
                    rdr = cmd.ExecuteReader();
                    if (rdr.Read())
                    {

                        //return rdr.GetString(0);
                        var x = JObject.Parse(rdr.GetString(0));
                        var Rahasia = x["Rahasia"];
                        return Rahasia.ToString();
                    }
                    else
                    {
                        return @"{""info"":""Please be Nice, sweetie.""}";
                    }
                }

            }
            catch (SqlException e)
            {
                return e.ToString();
            }

        }


    }
}

